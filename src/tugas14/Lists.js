import React, { useState,useEffect} from 'react';
import axios from "axios"
const List = () => {
const [dataBuah, setDataBuah] =  useState(null)
const [inputNama, setInputNama]  =  useState("")
const [inputHarga, setInputHarga]  =  useState("")
const [inputBerat, setInputBerat]  =  useState("")
const [selectedId, setSelectedId] =  useState(0)   
const [statusForm, setStatusForm]  =  useState("create")

useEffect( () => {
  if(dataBuah===null){
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(res => {
      setDataBuah(res.data.map(el=>{ return{id:el.id,nama:el.name,harga:el.price,berat:el.weight}}))
    })
  }
 
},[dataBuah])

const handleDelete=(event)=>{
  let id = parseInt(event.target.value)
  let newdata = dataBuah.filter(el => el.id != id)

  axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
  .then(res => {
    console.log(res)
  })
    setDataBuah([...newdata])
  }
  

const handleEdit = (event) =>{
  let id = parseInt(event.target.value)
  let data = dataBuah.find(x=> x.id === id)
  setInputNama(data.nama)
  setInputHarga(data.harga)
  setInputBerat(data.berat)
  setSelectedId(id)
  setStatusForm("edit")

  
}


const handleChangeNama=(event)=>{
  setInputNama(event.target.value)
}
const handleChangeHarga=(event)=>{
  setInputHarga(event.target.value)
}
const handleChangeBerat=(event)=>{
  setInputBerat(event.target.value)
}
const handleSubmit = (event) =>{
  // menahan submit
  event.preventDefault()

  let name=inputNama
  let weight=inputBerat
  let price=inputHarga

  if (name.replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g, '') 
    !== "" && price.toString().replace(/\s/g, '') !== ""){      
    if (statusForm === "create"){        
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name,price,weight})
      .then(res => {
        console.log(res)
          setDataBuah([...dataBuah, {id: res.data.id, nama: name, harga:price, berat:weight}])
      })
    }else if(statusForm === "edit"){
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name,price,weight})
      .then(res => {
        console.log(res)
          let databuah = dataBuah.find(el=> el.id === selectedId)

          databuah.nama = name
          databuah.berat = weight
          databuah.harga = price
          setDataBuah([...dataBuah])
        
      })
    }
    
    setStatusForm("create")
    setSelectedId(0)
    setInputNama("")
    setInputHarga("")
    setInputBerat("")
  }

}


return(
  <>
    <h1 style={{textAlign:"center"}}>Tugas 14</h1>
              <table style={{border: "2px solid black",marginLeft: "auto",marginRight: "auto"}}>
                  <tr style={{backgroundColor:"#ccc7c4"}}>
                    <th>no</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Berat</th>
                  <th>Aksi</th>
                  </tr>
                      {dataBuah !== null && dataBuah.map((el,index)=> {
                      return (
                          <tr style={{backgroundColor:"#f67833"}}>
                             <td>{index+1}</td>
                              <td style={{paddingRight:"400px"}}>{el.nama}</td>
                              <td style={{paddingRight:"100px"}}>{el.harga}</td>
                              <td style={{paddingRight:"100px"}}>{el.berat/1000} kg</td>
                              <td > 
                                 <button onClick={handleEdit} value={el.id}> Edit </button>
                                &nbsp;
                                 <button onClick={handleDelete} value={el.id}>Delete</button> 

                              </td>
                          </tr>
                      )})}
               </table> 
    {/* Form */}

    <h1 style={{textAlign:"center"}}>Form DataBuah</h1>
    <center>
    
    
    <form onSubmit={handleSubmit}>
        <table>
        <tbody>
            <tr>
            <td><label> Nama: </label></td>
                <td><input type="text" value={inputNama} onChange={handleChangeNama}/></td>
            </tr>
            <tr>
            <td><label> Harga: </label></td>
                <td><input type="number" value={inputHarga} onChange={handleChangeHarga}/></td>
            </tr>
            <tr>
            <td><label> Berat(g): </label></td>
                <td><input type="number" value={inputBerat} onChange={handleChangeBerat}/></td>
            </tr>
            <td></td>
            <td style={{ marginLeft: "0px" }}><button>submit</button></td>
            </tbody>
        </table>
    </form>


    </center>
  
  </>
)
                      }





export default List
