import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import TabelHargaBuah from '../tugas11/TabelHargaBuah';
import Timer from '../tugas12/Timer';
import Action from '../tugas13/Action';
import List from '../tugas14/Lists';
import Fruit from './Fruit';


const Routes = () => {

  return (
    <div >
    <Router>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

      <Switch>
        <Route exact path="/">
          <Fruit />
        </Route>
        <Route exact path="/tugas11">
          <TabelHargaBuah />
        </Route>
        <Route exact path="/tugas12">
          <Timer start={100}/>
        </Route>
        <Route exact path="/tugas13">
          <Action />
        </Route>
        <Route exact path="/tugas14">
          <List />
        </Route>
        <Route exact path="/tugas15">
          <Fruit />
        </Route>
      </Switch>
      </nav>
    </Router>
    </div>
  );
};

export default Routes;
