import React, {useContext} from "react"
import {Link} from "react-router-dom";
import {ColorContext} from "./ColorContext";

const NavColorList = () =>{
  const [color, setColor] = useContext(ColorContext);

  return(
    <>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="collapse navbar-collapse" id="navbar-SupportedContent">
      <ul style={{textAlign:"center"}}>
        <li style={{backgroundColor: color[Math.floor(Math.random()*color.length)]}}>
          <Link to="/">Home</Link>
        </li>
        <li style={{backgroundColor: color[Math.floor(Math.random()*color.length)]}}>
          <Link to="/tugas11">tugas11</Link>
        </li>
        <li style={{backgroundColor: color[Math.floor(Math.random()*color.length)]}}>
          <Link to="/tugas12">tugas12</Link>
        </li>
        <li style={{backgroundColor: color[Math.floor(Math.random()*color.length)]}}>
          <Link to="/tugas13">tugas13</Link>
        </li>
        <li style={{backgroundColor: color[Math.floor(Math.random()*color.length)]}}>
          <Link to="/tugas14">tugas14</Link>
        </li>
        <li style={{backgroundColor: color[Math.floor(Math.random()*color.length)]}}>
          <Link to="/tugas15">tugas15</Link>
        </li>
      </ul>
      <br></br>
    </div>
    </nav>
    </>
  )

}

export default NavColorList
