import React, { useState, createContext } from "react";

export const ColorContext = createContext();

export const ColorProvider = props => {
  const [color, setColor] =  useState(["#cae0ed", "#ddd", "#ebf7ff", "#badff5", "#afc1cc"])

  return (
    <ColorContext.Provider value={[color, setColor]}>
        {props.children}
    </ColorContext.Provider>
  );
};
