import React, {useContext} from "react"
import {FruitContext} from "./FruitContext"
import axios from "axios";



const FruitList = () =>{
  const [
          dataBuah, setDataBuah,
          inputNama, setInputNama,
          inputHarga, setInputHarga,
          inputBerat, setInputBerat,
          selectedId, setSelectedId,
          statusForm, setStatusForm
  ] = useContext(FruitContext);

  const handleEdit = (event) =>{
    let id = parseInt(event.target.value)
  let data = dataBuah.find(x=> x.id === id)
  setInputNama(data.nama)
  setInputHarga(data.harga)
  setInputBerat(data.berat)
  setSelectedId(id)
  setStatusForm("edit")

  }

  const handleDelete = (event) => {
    let id = parseInt(event.target.value)
  let newdata = dataBuah.filter(el => el.id != id)

  axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
  .then(res => {
    console.log(res)
  })
    setDataBuah([...newdata])
  }


  return(
   <>
      <h1 style={{textAlign:"center"}}>Table Harga Buah (Tugas 15)</h1>
      <div>
      <center>
      <table style={{border:"1px solid black"}} width="70%">
    <thead>
      <tr style={{backgroundColor:"#ccc7c4"}}>
        <th>No</th>
        <th>Nama</th>
        <th>Harga</th>
        <th>Berat</th>
        <th>Aksi</th>
      </tr>
      </thead>
      <tbody>
      {
           dataBuah !== null && dataBuah.map((el, index)=>{
             return(
               <tr style={{backgroundColor:"#f67833"}}>
                 <td>{index+1}</td>
                 <td>{el.nama}</td>
                 <td>{el.harga}</td>
                 <td>{el.berat/1000} kg</td>
                 <td style={{textAlign:"center"}}>
                    <button onClick={handleEdit} value={el.id}>Edit</button>
                    &nbsp;
                    <button onClick={handleDelete} value={el.id}>Delete</button>
                </td>
               </tr>
             )
           })
       }
      </tbody>
    </table>
    </center>
    </div>
  </>
  )
}


export default FruitList;
