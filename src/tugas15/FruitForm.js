import React, {useContext} from "react";
import {FruitContext} from "./FruitContext";
import axios from 'axios';

const FruitForm = () =>{
  const [
          dataBuah, setDataBuah,
          inputNama, setInputNama,
          inputHarga, setInputHarga,
          inputBerat, setInputBerat,
          selectedId, setSelectedId,
          statusForm, setStatusForm
  ] = useContext(FruitContext);


  const handleSubmit = (event) => {
    // menahan submit
    event.preventDefault()

  let name=inputNama
  let weight=inputBerat
  let price=inputHarga

  if (name.replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g, '') 
    !== "" && price.toString().replace(/\s/g, '') !== ""){      
    if (statusForm === "create"){        
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name,price,weight})
      .then(res => {
        console.log(res)
          setDataBuah([...dataBuah, {id: res.data.id, nama: name, harga:price, berat:weight}])
      })
    }else if(statusForm === "edit"){
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name,price,weight})
      .then(res => {
        console.log(res)
          let databuah = dataBuah.find(el=> el.id === selectedId)

          databuah.nama = name
          databuah.berat = weight
          databuah.harga = price
          setDataBuah([...dataBuah])
        
      })
    }
    
    setStatusForm("create")
    setSelectedId(0)
    setInputNama("")
    setInputHarga("")
    setInputBerat("")
  }

  }

  const handleChangeNama=(event)=>{
  setInputNama(event.target.value)
}
const handleChangeHarga=(event)=>{
  setInputHarga(event.target.value)
}
const handleChangeBerat=(event)=>{
  setInputBerat(event.target.value)
}

  return(
    <>
      
      <center>
      <h1>Form Data Buah</h1>
      <form onSubmit={handleSubmit}>
         <table>
        <tbody>
            <tr>
            <td><label> Nama: </label></td>
                <td><input type="text" value={inputNama} onChange={handleChangeNama}/></td>
            </tr>
            <tr>
            <td><label> Harga: </label></td>
                <td><input type="number" value={inputHarga} onChange={handleChangeHarga}/></td>
            </tr>
            <tr>
            <td><label> Berat(g): </label></td>
                <td><input type="number" value={inputBerat} onChange={handleChangeBerat}/></td>
            </tr>
            <td></td>
            <td><button>submit</button></td>
            </tbody>
        </table>
    </form>
    </center>
    </>
  )

}

export default FruitForm
