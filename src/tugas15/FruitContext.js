import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const FruitContext = createContext();

export const FruitProvider = props => {
const [dataBuah, setDataBuah] =  useState(null)
const [inputNama, setInputNama]  =  useState("")
const [inputHarga, setInputHarga]  =  useState("")
const [inputBerat, setInputBerat]  =  useState("")
const [selectedId, setSelectedId] =  useState(0)   
const [statusForm, setStatusForm]  =  useState("create")


  useEffect(() => {
    if (dataBuah === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
          setDataBuah(res.data.map(el=>{ return{id:el.id,nama:el.name,harga:el.price,berat:el.weight}}))
        })
    }
  })

  return (
    <FruitContext.Provider value={
        [
          dataBuah, setDataBuah,
          inputNama, setInputNama,
          inputHarga, setInputHarga,
          inputBerat, setInputBerat,
          selectedId, setSelectedId,
          statusForm, setStatusForm
        ]
    }>
        {props.children}
    </FruitContext.Provider>
  );
};
