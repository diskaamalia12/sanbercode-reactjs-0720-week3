import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class Action extends React.Component {
    constructor(props){
        super(props);
        this.state = {daftar: dataHargaBuah, formName: "tambah"}

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    handleChange(e){
        let nam = e.target.name;
        if( nam === "nama" ){
            this.setState({inputName: e.target.value});
        }else if( nam === "harga" ){
            this.setState({inputPrice: e.target.value});
        }else{
            this.setState({inputWeight: e.target.value});
        }
    }
  
    handleSubmit(event){
      event.preventDefault()
      if( this.state.formName === "tambah" ){
        this.setState({
            daftar: [...this.state.daftar,{ nama: this.state.inputName, harga: this.state.inputPrice, berat: this.state.inputWeight }],
            inputName: "",
            inputPrice: "",
            inputWeight: "",
        })
      }else{
        //edit form
        let newArr = this.state.daftar;
        newArr[this.state.editedIndex] = { nama: this.state.inputName, harga: this.state.inputPrice, berat: this.state.inputWeight }
        this.setState({
            daftar: newArr,
            formName: "tambah",
            inputName: "",
            inputPrice: "",
            inputWeight: "",
        });
      }
    }
    
    // componentDidMount(){
    // }
    
    // componentDidUpdate(){
    // }

    handleDelete(e){
        let newArr = [...this.state.daftar];
        newArr.splice(e.target.getAttribute('index'), 1);
        this.setState({
            daftar: newArr
        });
    }

    handleEdit(e){
        let index = e.target.getAttribute('index');
        this.setState({
            editedIndex: index,
            formName: "edit",
            inputName: this.state.daftar[index].nama,
            inputPrice: this.state.daftar[index].harga,
            inputWeight: this.state.daftar[index].berat,
        });
    }

    render(){
        return (
            <div>
            <center>
            <h1>Tugas 13</h1>
            <table className="tabel1">
                <thead>
                    <tr>
                        <td>Nama</td>
                        <td>Harga</td>
                        <td>Berat</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    {this.state.daftar.map((el,index)=> {
                    return (
                        <tr key={index}>
                            <td>{el.nama}</td>
                            <td>{el.harga}</td>
                            <td>{(el.berat/1000)+" kg"}</td>
                            <td>
                                <a onClick={this.handleDelete} index={index} href="#">delete</a>
                                <span>  </span>
                                <a onClick={this.handleEdit} index={index} href="#">edit</a>
                            </td>
                        </tr>
                    )
                    })}
                    {/* <tr>
                        <td >Tambah</td>
                    </tr> */}
                </tbody>
            </table>
            
            {/* Form */}
            <h1>Form Data Buah</h1>
            <form onSubmit={this.handleSubmit} className="form-tugas13">
                <table>
                    <tbody>
                        <tr>
                            <td><label>Masukkan nama buah:</label></td>
                            <td><input type="text" name="nama" value={this.state.inputName} onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td><label>Masukkan harga buah:</label></td>
                            <td><input type="number" name="harga" value={this.state.inputPrice} onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td><label>Masukkan berat buah (g):</label></td>
                            <td><input type="number" name="berat" value={this.state.inputWeight} onChange={this.handleChange}/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right"><button>{this.state.formName}</button></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            </center>
            </div>
        )
    }
}

export default Action
