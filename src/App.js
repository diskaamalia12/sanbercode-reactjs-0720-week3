import React from 'react';
import './App.css';

import { BrowserRouter as Router } from "react-router-dom";
import Routes from './tugas15/Routes';
import Nav from './tugas15/NavColor';


function App() {
  return (
    <>
      
    <Router>
      <Nav />
      <Routes />
    </Router>
   </>
  );
}

export default App;
